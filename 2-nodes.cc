/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 The Boeing Company
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Pelaez, David. david_hernan.pelaez_feijoo@smail.th-koeln.de
 *
 * A simple example with 2 802.11g nodes
 *
 */

#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"

#include "ns3/applications-module.h"
#include "ns3/netanim-module.h"
#include "ns3/flow-monitor-module.h"

#include "ns3/olsr-routing-protocol.h"
#include "ns3/olsr-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("2-nodes-Wifi-Adhoc");

int main (int argc, char *argv[])
{
  /******************************************************************
   *                    General Parameters                          *
   * ***************************************************************/

  uint16_t nNodes = 2;
  double distance = 5.0; // in meters
  double datarate = 54; // in Mbps
  double convtime = 60;
  double testTime = 14;
  std::string app = "echo";
  uint32_t packetsize = 1400; // in bytes
  bool pcap = false;
  //bool animator = false;
  std::string logFile("mylog");
  bool csv = false;

  /******************************************************************
   *                     Command line arguments                     *
   * ***************************************************************/

  CommandLine cmd;
  cmd.AddValue ("nodes","Number of nodes in Ad-Hoc network", nNodes);
  cmd.AddValue ("d","Distance between nodes", distance);
  cmd.AddValue ("pcap", "Enable pcap in all nodes", pcap);
  cmd.AddValue ("app", "Select type of application to run", app);
  cmd.AddValue ("convergence", "Enough time for topology to converge", convtime);
  cmd.AddValue ("ttime", "Timelength of test", testTime);
  cmd.AddValue ("csv", "Use a csv file to save the results", csv);
  cmd.AddValue ("datarate", "Test diferent values of data rate", datarate);
  //cmd.AddValue ("animator", "Enable NetAnim", animator);

  cmd.Parse (argc, argv); //Parse the commnad line arguments

  //LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
  //LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
  LogComponentEnable ("2-nodes-Wifi-Adhoc", LOG_LEVEL_INFO);
  //LogComponentEnable ("UdpServer", LOG_LEVEL_INFO);
  //LogComponentEnable ("UdpClient", LOG_LEVEL_INFO);

  /**********************************************************************
   *                Node creation and network setup                     *
   * *******************************************************************/


  NS_LOG_INFO ("Configuring wifi characteristics");

  NodeContainer wNodes;
  wNodes.Create (nNodes);

  /*********** Set up of channel characteristics **********************/

  WifiHelper wifi;
  wifi.SetStandard (WIFI_PHY_STANDARD_80211g);
  wifi.SetRemoteStationManager ("ns3::MinstrelWifiManager");

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel", "Frequency", DoubleValue(2.4e+09));

  YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();

  /********************************************************************
   *              Physical characteristics of tx                      *
   * *****************************************************************/

  wifiPhy.SetErrorRateModel ("ns3::YansErrorRateModel");
  wifiPhy.Set ("TxGain", DoubleValue(1.0));
  wifiPhy.Set ("RxGain", DoubleValue(1.0));
  wifiPhy.Set ("TxPowerStart", DoubleValue(1.0));
  wifiPhy.Set ("TxPowerEnd", DoubleValue(1.0));
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO); //enable pcap generation
  wifiPhy.SetChannel (wifiChannel.Create ());

  WifiMacHelper wifiMac;
  wifiMac.SetType ("ns3::AdhocWifiMac"); //Define that the node works in Ad-hoc mode

  NetDeviceContainer devices  = wifi.Install (wifiPhy, wifiMac, wNodes);

  /********************************************************************
   *                  Positions of the nodes                          *
   * *****************************************************************/
  NS_LOG_INFO ("Seting up mobility");

  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (distance),
                                 "DeltaY", DoubleValue (0.0),
                                 "GridWidth", UintegerValue (5),
                                 "LayoutType", StringValue ("RowFirst"));

  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wNodes);

  /************** Enable OLSR routing *****************************/
  OlsrHelper olsr; //This class is expected to be used in conjunction with ns3::InternetStackHelper::SetRoutingHelper
  Ipv4StaticRoutingHelper staticRouting;

  Ipv4ListRoutingHelper list;
  list.Add (staticRouting, 0);
  list.Add (olsr, 10); //Input routing helper and priority, used later to create a list routing

  NS_LOG_INFO ("Seting up Internet stack");

  InternetStackHelper internet;
  internet.SetRoutingHelper (list); // has effect on the next Install ()
  internet.Install (wNodes);

  // IP Addreses
  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaces = ipv4.Assign (devices); //assign v4 addresses to all interfaces

  NS_LOG_INFO ("Starting Applications");

  //Applications
  if (app=="echo"){
      UdpEchoServerHelper echoServer (9);//Use port number 9
      ApplicationContainer serverApps = echoServer.Install (wNodes.Get (0));
      serverApps.Start (Seconds (convtime));
      serverApps.Stop (Seconds (convtime + testTime));

      UdpEchoClientHelper echoClient (interfaces.GetAddress (0), 9);
      /*we are creating the helper and telling it so set the remote address of
       * the client to be the IP address assigned to the node on which the server resides.
       * We also tell it to arrange to send packets to port nine. */

      echoClient.SetAttribute ("MaxPackets", UintegerValue (1));
      echoClient.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
      echoClient.SetAttribute ("PacketSize", UintegerValue (packetsize));

      ApplicationContainer clientApps = echoClient.Install (wNodes.Get (nNodes-1));
      clientApps.Start (Seconds (convtime + 3.0));
      clientApps.Stop (Seconds (convtime + testTime));

  } else if (app=="udp"){
    // Create one udpServer applications on node one.
    //
      NS_LOG_INFO ("Starting Udp flow application");
       UdpServerHelper server (9);
       ApplicationContainer apps = server.Install (wNodes.Get (nNodes-1));
       apps.Start (Seconds (convtime));
       apps.Stop (Seconds (convtime + testTime));
       //
       // Create one UdpClient application to send UDP datagrams from node zero to
       // node 4.
       //
       //uint32_t MaxPacketSize = 1400;
       //uint32_t maxPacketCount = 9876543210u; //71429
       UdpClientHelper client (interfaces.GetAddress (nNodes-1), 9);
       client.SetAttribute ("MaxPackets", UintegerValue (4294967295u));
       double pps = (datarate * 1000000) / (packetsize * 8);
       client.SetAttribute ("Interval", TimeValue (Time (Seconds (1 / pps))));
       client.SetAttribute ("PacketSize", UintegerValue (packetsize));
       apps = client.Install (wNodes.Get (0));
       apps.Start (Seconds (convtime + 1.0));
       apps.Stop (Seconds (convtime + testTime));

    } else if (app=="tcp") {
      NS_LOG_INFO ("Creating TCP application..");
      Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (packetsize));
      OnOffHelper source ("ns3::TcpSocketFactory", InetSocketAddress (interfaces.GetAddress (nNodes-1), 80));
      source.SetAttribute ( "OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
      source.SetAttribute ( "OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
      source.SetAttribute ("DataRate", DataRateValue (datarate * 1000000));
      ApplicationContainer apps;
      apps.Add (source.Install (wNodes.Get (0)));

      // Configure the Server on the destination node.
      PacketSinkHelper TCPsink ("ns3::TcpSocketFactory",  InetSocketAddress (interfaces.GetAddress (nNodes-1), 80));
      Config::SetDefault ("ns3::TcpL4Protocol::SocketType", StringValue ("ns3::TcpNewReno")); //Check type of TCP
      apps.Add (TCPsink.Install (wNodes.Get (nNodes-1)));

      apps.Start (Seconds (convtime + 1.0));
      apps.Stop (Seconds (convtime + testTime));
    }

  if (pcap){
      //wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
      //wifiPhy.EnablePcap ("2-nodes-");
      wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
      std::stringstream stream;
      stream << "two-nodes";
      wifiPhy.EnablePcap (stream.str (), devices);
    }

  AnimationInterface anim ("2-nodes.xml");
  anim.EnableIpv4RouteTracking ("2-nodes-route-tracking.xml",Seconds(0.0),Seconds(convtime + testTime),wNodes,Seconds (0.1));

  // Install FlowMonitor on all nodes
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll ();


  //Run the simulation
  Simulator::Stop (Seconds (convtime + testTime)); //Check later to stablish this value to the correct time
  Simulator::Run ();
  Simulator::Destroy ();

  NS_LOG_INFO ("Done...");

  // Print per flow statistics
  monitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats ();

  Time runTime;
  runTime = Seconds (testTime);
  std::ofstream results;
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
    {
      //
      //
      //
      if (i->first > 0)
        {
          Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
          if (csv == true){
                            std::cout << nNodes-1<< ",";
                            std::cout << i->first << ",";
                            std::cout << t.sourceAddress << ",";
                            std::cout << t.destinationAddress << ",";
                            std::cout << testTime << ",";
                            std::cout << i->second.txPackets << ",";
                            std::cout << i->second.txBytes << ",";
                            std::cout << i->second.txBytes * 8.0 / (runTime.GetSeconds ()) / 1000 / 1000 << ",";
                            std::cout << i->second.rxPackets << ",";
                            std::cout << i->second.rxBytes << ",";
                            std::cout << i->second.rxBytes * 8.0 / (runTime.GetSeconds ()) / 1000 / 1000 << ",";
                            std::cout << distance << ",";
                            std::cout << "\n";
                            if (logFile.size () > 0)
                              {
                                results.open (logFile.c_str (), std::ios::out | std::ios::app);
                                if (results.is_open ())
                                  {
                                    results << nNodes-1<< ",";
                                    results << i->first << ",";
                                    results << t.sourceAddress << ",";
                                    results << t.destinationAddress << ",";
                                    results << testTime << ",";
                                    results << i->second.txPackets << ",";
                                    results << i->second.txBytes << ",";
                                    results << i->second.txBytes * 8.0 / (runTime.GetSeconds ()) / 1000 / 1000  << ",";
                                    results << i->second.rxPackets << ",";
                                    results << i->second.rxBytes << ",";
                                    results << i->second.rxBytes * 8.0 / (runTime.GetSeconds ()) / 1000 / 1000 << ",";
                                    results << distance << ",";
                                    results << "\n";
                                    results.close ();
                                  }
                              }
            }
          else {
              std::cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
              std::cout << "  Tx Packets: " << i->second.txPackets << "\n";
              std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
              std::cout << "  TxOffered:  " << i->second.txBytes * 8.0 / (runTime.GetSeconds ()) / 1000 / 1000  << " Mbps\n";
              std::cout << "  Rx Packets: " << i->second.rxPackets << "\n";
              std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
              std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / (runTime.GetSeconds ()) / 1000 / 1000  << " Mbps\n";
              std::cout << "  Hops: " << nNodes-1 << "\n" ;
              std::cout << "  Distance: " << distance << " m\n";
              std::cout << "*********************************************" << "\n";
            }

        }
    }

  return 0;

}
